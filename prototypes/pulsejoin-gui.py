#!/usr/bin/env python3
# based on https://gist.github.com/marvin/4323385
# examples from redhat gtk page

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class LabelWindow(Gtk.Window):

	def __init__(self):
		Gtk.Window.__init__(self, title="PulseJoin")
		
		hbox = Gtk.Box(spacing=10)
		hbox.set_homogeneous(False)
		vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
		vbox_left.set_homogeneous(False)
		vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
		vbox_right.set_homogeneous(False)

		hbox.pack_start(vbox_left, True, True, 0)
		hbox.pack_start(vbox_right, True, True, 0)

		label = Gtk.Label()
		label.set_markup("Скрипт <b>PulseJoin</b> позволяет легко <b>записать звук с микрофона и динамиков одновременно в программах, не умеющих записывать с нескольких устройств сразу</b>.\n"
		                 "\n"
		                 "Нужно выбрать режим работы.\n"
		                 ""
		                 "Можно <i>все сделать автоматически</i>:\n"
		                 "1) создать виртуальный микрофон, объединяющий в себе звук с устройства ввода (микрофона) по умолчанию и устройства вывода по умолчанию (что выводится на динамики/наушники)\n"
		                 "2) открыть Audacity для записи звука с этого объединенного виртуального устройства\n"
		                 "3) убрать это виртуальное устройство после закрытия Audacity\n")
		label.set_line_wrap(True)
		vbox_left.pack_start(label, True, True, 0)
		
		button = Gtk.Button(label="Сделать все автоматически с запуском Audacity!")
		vbox_left.pack_start(button, True, True, 0)
		button.connect("clicked", self.print_hello_world)
		
		label = Gtk.Label("Можно каждое действие выполнить по-отдельности с помощью кнопок:")
		label.set_line_wrap(True)
		vbox_left.pack_start(label, True, True, 0)
		
		button = Gtk.Button(label="Сделать виртуальный микрофон")
		# image=Gtk.Image(icon-name="audio-input-microphone")
		vbox_left.pack_start(button, True, True, 0)
		button.connect("clicked", self.print_hello_world)
		
		button = Gtk.Button(label="Убрать виртуальный микрофон")
		vbox_left.pack_start(button, True, True, 0)
		button.connect("clicked", self.print_hello_world)
				
		button = Gtk.Button(label="Print Hello")
		#label.set_mnemonic_widget(button)
		vbox_left.pack_start(button, True, True, 0)
		button.connect("clicked", self.print_hello_world2)
		
		self.add(hbox)
		
	def print_hello_world(self, widget):
		print("Hello World!")
		
	def print_hello_world2(self, widget):
		print("Hello World2!")

window = LabelWindow()        
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
