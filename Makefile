#
prefix ?= /usr
localedir ?= $(prefix)/share/locale
bindir ?= $(prefix)/bin
datadir ?= $(prefix)/share

all:
	@ echo "Nothing to compile. Use: make install"

install:
	install -d $(DESTDIR)/$(bindir)
	install -d $(DESTDIR)/$(datadir)/applications
	install -d $(DESTDIR)/$(localedir)/ru/LC_MESSAGES
	install -d $(DESTDIR)/$(localedir)/es/LC_MESSAGES
	install -m0755 ./pulsejoin.sh $(DESTDIR)/$(bindir)/pulsejoin
	install -m0644 ./pulsejoin.desktop $(DESTDIR)/$(datadir)/applications/
	msgfmt -o $(DESTDIR)/$(localedir)/ru/LC_MESSAGES/pulsejoin.mo po/ru.po
	msgfmt -o $(DESTDIR)/$(localedir)/es/LC_MESSAGES/pulsejoin.mo po/es.po
 
