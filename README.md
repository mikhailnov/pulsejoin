# PulseJoin

GUI для создания виртуального микрофона PulseAudio, с которого можно записывать звук одновременно настоящего микрофона и тот, что выводится на динамики.

GUI for making a virtual PulseAudio microphone, from which sound from both microphone and audio output can be recorded.

![](img/img1.png)

![](img/img3.png) 

![](img/img4.png) 

## Пример использования / Usage example

![](img/img2.png)

## Установка / Installation

### Установка на Ubuntu / Installing on Ubuntu

```
sudo add-apt-repository ppa:mikhailnov/pulsejoin -y
sudo apt update
sudo apt install pulsejoin --install-recommends
```

Для работы [рекомендуется](https://github.com/wwmm/pulseeffects/issues/99 "PulseEffects Issue #99"), но [не обязателен](https://gitlab.com/mikhailnov/pulsejoin/blob/master/pulsejoin.sh#L55) PulseAudio >= 12. Если у вас Ubuntu 18.04, то в ней PulseAudio 11, а для установки PulseAudio 12 на Ubuntu 18.04 сделайте:

We [recommend](https://github.com/wwmm/pulseeffects/issues/99 "PulseEffects Issue #99"), but [do not require](https://gitlab.com/mikhailnov/pulsejoin/blob/master/pulsejoin.sh#L55) PulseAudio >= 12. If you are using 18.04, then you have PulseAudio 11, to install PulseAudio 12 on Ubuntu 18.04 do:

```
sudo add-apt-repository ppa:mikhailnov/pulseeffects -y
sudo apt dist-upgrade
```

### Установка на Debian / Installing on Debian

Let's add repository from Ubuntu with priority 1 (see `man apt_preferences` about priorities) and install package from there. The package `pulsejoin` is not binary and so is fully compatible Debian/Deepin.

```
echo "deb http://ppa.launchpad.net/mikhailnov/pulsejoin/ubuntu bionic main" | sudo tee /etc/apt/sources.list.d/mikhailnov-ubuntu-pulsejoin-bionic.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys FE3AE55CF74041EAA3F0AD10D5B19A73A8ECB754 
echo -e "Package: * \nPin: release o=LP-PPA-mikhailnov-pulsejoin \nPin-Priority: 1" | sudo tee /etc/preferences.d/mikhailnov-ubuntu-pulsejoin-ppa
sudo apt update
sudo apt install pulsejoin
```

### Установка на Arch Linux / Installing on Arch Linux

AUR: [pulsejoin](https://aur.archlinux.org/packages/pulsejoin/)

### Установка на ALT Linux / Installing on ALT Linux

`pulsejoin` доступен в репозитории ALT Linux Sysiphus и поддерживается в нем автором PulseJoin, поэтому пользователям ALT Linux Sysiphus достаточно выполнить `apt-get install pulsejoin`.

`pulsejoin` is available in ALT Linux Sysiphus and is maintained by the author of PulseJoin, that's why ALT Linux Sysiphus users can install it by running `apt-get install pulsejoin`.

Пакет *.rpm для ALT Linux можно скачать в [тегах](https://gitlab.com/mikhailnov/pulsejoin/tags "Tags"). Для установки в папке со скачанным пакетом выполните:  
`su -c "apt-get install ./pulsejoin*.rpm"`

*.rpm package for ALT Linux can be downloaded from [tags](https://gitlab.com/mikhailnov/pulsejoin/tags "Tags"). To install it run in the directory with the downloaded package:  
`su -c "apt-get install ./pulsejoin*.rpm"`

### Установка на ROSA Fresh / Installing on ROSA Fresh

PulseJoin есть в репозитории ROSA Fresh и обновляется там автором PulseJoin. Установка:  
`sudo urpmi pulsejoin`

PulseJoin is availailbale in repositories of ROSA Fresh and is maintained by the author of PulseJoin. To install it run:  
`sudo urpmi pulsejoin`

### Установка на другие дистрибутивы / Installing on other systems

PulseJoin должен работать в любой GNU/Linux или BSD системе.

PulseJoin should work in any GNU/Linux or BSD system.

Для работы нужны: **yad**, bash, pulseaudio.

Dependencies: **yad**, bash, pulseaudio.

Установка вручную / manual installation:
```
git clone https://gitlab.com/mikhailnov/pulsejoin
cd pulsejoin
sudo make install
```

## Сборка пакетов / Building packages

### Сборка Deb-пакета / Building Deb-package

```
git clone https://gitlab.com/mikhailnov/pulsejoin
cd pulsejoin
dpkg-buildpackage; debian/rules clean
sudo apt install ../pulsejoin*.deb
```

### Сборка RPM-пакета на ALT Linux / Building RPM package on ALT Linux

```
git clone https://gitlab.com/mikhailnov/pulsejoin
cd pulsejoin
rpmbb # должен быть установлен пакет etersoft-build-utils
su -c "apt-get install $HOME/RPM/RPMS/noarch/pulsejoin-*.rpm"
```

